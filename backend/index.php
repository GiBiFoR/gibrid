<?php
use app\modules\user\models\User;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $league app\modules\league\models\League */
/* @var $user app\modules\user\models\User */

$this->title = 'Sportspring';
$league = Yii::$app->get('leagueManager')->getIdentity();
$user = Yii::$app->user->identity;
$isAdmin = $user->checkRole(User::ROLE_SUPER_ADMIN | User::ROLE_ADMIN);
?>
<script type="text/javascript" src="//vk.com/js/api/openapi.js?150"></script>
<script type="text/javascript">
    VK.Widgets.Group("vk_groups", {mode: 4, wide: 1, width: "auto", height: "700"}, 48020798);
</script>
<style>
    .info ul {
      margin: 0;
      padding-left: 16px;
    }

    .info ul li {
        margin-bottom: 24px;
        font-size: 18px;
    }

    .info ul li a {
        display: inline-block;
        position: relative;
        margin-left: 4px;
        padding-left: 2px;
        margin-top: -5px;
        font-size: 16px !important;
    }

    .info ul li a:before {
        font-size: 14px;
    }

    .info ul li .text-muted {
        font-size: 12px;
        padding-top: 4px;
        color: #d5d5d5;
    }

    .info ul li .text-muted .fa.fa-info-circle {
        color: #ffffff;
        margin-right: 4px;
    }

    .widget-row {
        margin-top: 15px;
        margin-left: 0;
    }

    .panel:not(.panel-dark, .panel-mint) .panel-body a {
        color: #38a0f4;
    }
</style>
<?php if ($league->demo_mode || $league->demo_lock) { ?>
    <?php echo $this->render('_demo-panel', [
        'league' => $league,
    ]); ?>
    <br>
<?php } ?>

<?php if($league->isExpiredSoon()) { ?>
    <?php echo $this->render('_expired-soon-panel'); ?>
    <br>
<?php } ?>

<div class="panel-alert">
    <div class="alert-wrap in">
        <div class="alert alert-primary">
            <?php echo Yii::t('app','Инструкции, подсказки и ответы на часто задаваемые вопросы​ смотрите'); ?> <a target="_blank" href="http://wiki.join.football"><?php echo Yii::t('app','здесь');?></a>.
        </div>
    </div>
</div>
<br>

<div class="panel-alert">
    <div class="alert-wrap in">
        <div class="media mobile-media">
            <span style="font-size: 16px;">
                <?php if ($isAdmin) {
                    echo Yii::t('app','КРАТКОЕ РУКОВОДСТВО ПО НАЧАЛУ РАБОТЫ С СИСТЕМОЙ:');
                } else {
                    echo Yii::t('app','КРАТКОЕ РУКОВОДСТВО ДЛЯ АДМИНИСТРАТОРОВ КОМАНД ПО НАЧАЛУ РАБОТЫ С СИСТЕМОЙ:');
                } ?>
            </span>
        </div>
    </div>
</div>
<br>
<div class="row">
   <div class="col-sm-12">
        <?php if ($isAdmin) { ?>
            <div class="panel panel-colorful panel-dark">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo Yii::t('app','Для администраторов системы'); ?></h3>
                </div>
                <div class="panel-body info">
                    <ul style="margin: 0;">
                        <li>
                            <?php echo Yii::t('app','Ознакомьтесь с видео-инструкцией {0}', [
                                Html::a(Yii::t('app','здесь'), ['/tournament/backend/search/index'], ['class' => 'btn btn-xs btn-primary btn-labeled ti-link mobile-mrgn-top', 'target' => '_blank'])
                            ]); ?>
                            <p class="text-muted text-sm"><i class="fa fa-info-circle"></i><?php echo Yii::t('app','Как правильно создать турнир и сформировать календарь игр'); ?></p>
                        </li>
                        <li>
                            <?php echo Yii::t('app','Создайте турнир {0}', [
                                Html::a(Yii::t('app','здесь'), ['/tournament/backend/profile/create'], ['class' => 'btn btn-xs btn-primary btn-labeled ti-link mobile-mrgn-top', 'target' => '_blank'])
                            ]); ?>
                            <p class="text-muted text-sm"><i class="fa fa-info-circle"></i><?php echo Yii::t('app','Создайте и оформите турнир, укажите данные регламента и другие настройки'); ?></p>
                        </li>
                        <li>
                            <?php echo Yii::t('app','Создайте команды {0}', [
                                Html::a(Yii::t('app','здесь'), ['/contacts/backend/contact/index'], ['class' => 'btn btn-xs btn-primary btn-labeled ti-link mobile-mrgn-top', 'target' => '_blank'])
                            ]); ?>
                            <p class="text-muted text-sm"><i class="fa fa-info-circle"></i><?php echo Yii::t('app','Создайте и оформите команды. Создайте игроков, участников команд - кнопка "Состав" в анонсе команды в списке всех команд'); ?></p>
                        </li>
                        <li>
                            <?php echo Yii::t('app','Заявите команды на турнир {0}', [
                                Html::a(Yii::t('app','здесь'), ['/tournament/backend/search/index'], ['class' => 'btn btn-xs btn-primary btn-labeled ti-link mobile-mrgn-top', 'target' => '_blank'])
                            ]); ?>
                            <p class="text-muted text-sm"><i class="fa fa-info-circle"></i><?php echo Yii::t('app','Созданные ранее команды можно заявить на турнир - зеленая кнопка справа "Заявки". Заявите команды и игроков'); ?></p>
                        </li>
                        <li>
                            <?php echo Yii::t('app','Создайте группы в турнире'); ?>
                            <p class="text-muted text-sm"><i class="fa fa-info-circle"></i><?php echo Yii::t('app','Кнопка "Этапы" в анонсе турнира поможет создать групповой этап или этап типа "Плей-офф". Добавьте заявленные команды в созданный этап'); ?></p>
                        </li>
                        <li>
                            <?php echo Yii::t('app','Создайте календарь матчей'); ?>
                            <p class="text-muted text-sm"><i class="fa fa-info-circle"></i><?php echo Yii::t('app','В настройках этапа создайте календарь матчей вручную или автоматически. Укажите даты и время игр'); ?></p>
                        </li>
                        <li>
                            <?php echo Yii::t('app','Заполните протокол матча'); ?>
                            <p class="text-muted text-sm"><i class="fa fa-info-circle"></i><?php echo Yii::t('app','Внесите данные, отметив участников матча и игровую статистику'); ?></p>
                        </li>
                    </ul>
                </div>
            </div>
       <?php } else { ?>
            <div class="panel panel-colorful panel-dark">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo Yii::t('app','Для администраторов команд'); ?></h3>
                </div>
                <div class="panel-body info">
                    <ul style="margin: 0;">
                        <li>
                            <?php echo Yii::t('app','Проверьте {0}, не была ли создана ваша команда ранее кем-то другим', [
                                Html::a(Yii::t('app','здесь'), ['/team/backend/search/index'], ['class' => 'btn btn-xs btn-primary btn-labeled ti-link mobile-mrgn-top', 'target' => '_blank'])
                            ]); ?>
                            <p class="text-muted text-sm"><i class="fa fa-info-circle"></i><?php echo Yii::t('app','осуществите поиск по названию; поверьте разные варианты написания'); ?></p>
                        </li>
                        <li>
                            <?php echo Yii::t('app','Нажмите кнопку "Заявка на управление", если ваша команда была создана ранее'); ?>
                            <p class="text-muted text-sm"><i class="fa fa-info-circle"></i><?php echo Yii::t('app','и если вы хотите стать вторым администратором этой команды'); ?></p>
                        </li>
                        <li>
                            <?php echo Yii::t('app','Создайте свою команду {0}, если не нашли ее при проверке', [
                                Html::a(Yii::t('app','здесь'), ['/team/backend/profile/create'], ['class' => 'btn btn-xs btn-primary btn-labeled ti-link mobile-mrgn-top', 'target' => '_blank'])
                            ]); ?>
                            <p class="text-muted text-sm"><i class="fa fa-info-circle"></i><?php echo Yii::t('app','см. пункт №1'); ?></p>
                        </li>
                        <li>
                            <?php echo Yii::t('app','Создайте игроков в рамках своей команды'); ?>
                            <p class="text-muted text-sm"><i class="fa fa-info-circle"></i><?php echo Yii::t('app','нажмите кнопку "Состав", размещенную под названием команды, и создайте записи о всех своих игроках'); ?></p>
                        </li>
                        <li>
                            <?php echo Yii::t('app','Не забудьте указать себя в качестве игрока в составе своей команды'); ?>
                            <p class="text-muted text-sm"><i class="fa fa-info-circle"></i><?php echo Yii::t('app','Создайте и свой игровой профиль, по аналогии с другими игроками, если сами играете в составе команды'); ?></p>
                        </li>
                        <li>
                            <?php echo Yii::t('app','Найдите нужный турнир {0} и заявите свою команду и всех необходимых игроков', [
                                Html::a(Yii::t('app','здесь'), ['/tournament/backend/search/index'], ['class' => 'btn btn-xs btn-primary btn-labeled ti-link mobile-mrgn-top', 'target' => '_blank'])
                            ]); ?>
                            <p class="text-muted text-sm"><i class="fa fa-info-circle"></i><?php echo Yii::t('app','нажмите кнопку "Заявиться", размещенную справа от названия турнира'); ?></p>
                        </li>
                    </ul>
                </div>
            </div>
       <?php } ?>
   </div>
</div>
<?php if ($isAdmin) { ?>
    <br>
    <div class="panel-alert">
        <div class="alert-wrap in">
            <div class="media mobile-media">
                <span style="font-size: 16px;"><?php echo Yii::t('app','НАША ГРУППА В ВК. ПОДПИСЫВАЙТЕСЬ, ЗАДАВАЙТЕ ВОПРОСЫ'); ?></span>
            </div>
        </div>
    </div>
    <div class="row widget-row" style="max-width: 600px;">
        <div id="vk_groups"></div>
    </div>
<?php } ?>
